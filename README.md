# Store and Recall sample scripts

## Purpose:

This collection of script will provide a simple example of some shell scripting with a purpose.  The purpose is to provide the user with scratch pad to store commands and tips.  These tips and command examples can later be printed to the screen based on a query of the stored values using a grep function in the recall script.

### Contents

There are three files used in the initial version of this solution.
`store.sh`, `recall.sh` and **store.db**.

#### store.sh

store.sh script will store provided commands by writing them to a file called store.db.  While the file name includes .db it is just a text file.  

#### recall.sh

recall.sh will print to screen any lines found in store.db based on the query provided.  If no query is provided the default behaviour is to print all the contents of the store.db file.

#### store.db

store.db is not included by default but will be created on the first use of the store.sh script or it can be defined before running the script.  This repo does include a sameple store.db file that can be renamed from store.db.sample to store.db 

>__Note:__ The file extensions are not required, you can rename store.sh and recall.sh to just store and recall.

